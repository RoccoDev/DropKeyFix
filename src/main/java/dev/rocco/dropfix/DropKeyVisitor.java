package dev.rocco.dropfix;

import org.objectweb.asm.Label;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;

public class DropKeyVisitor extends MethodVisitor {

    public DropKeyVisitor(MethodVisitor mv) {
        super(Opcodes.ASM5, mv);
    }

    @Override
    public void visitCode() {
        super.visitCode();

        Label label = new Label();
        mv.visitMethodInsn(Opcodes.INVOKESTATIC, "dev/rocco/dropfix/DropKeyFixMod", "shouldDrop",
                "()Z", false);
        mv.visitJumpInsn(Opcodes.IFNE, label);
        mv.visitInsn(Opcodes.ACONST_NULL);
        mv.visitInsn(Opcodes.ARETURN);
        mv.visitLabel(label);
    }
}
