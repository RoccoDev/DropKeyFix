package dev.rocco.dropfix.cmd;

import net.minecraft.client.Minecraft;
import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ChatComponentText;

public class UnlocalizedCommand extends CommandBase {
    @Override
    public String getCommandName() {
        return "dropname";
    }

    @Override
    public String getCommandUsage(ICommandSender sender) {
        return null;
    }

    @Override
    public void processCommand(ICommandSender sender, String[] args) throws CommandException {
        ItemStack item = Minecraft.getMinecraft().thePlayer.getHeldItem();

        if(item == null) {
            Minecraft.getMinecraft().thePlayer.addChatComponentMessage(new ChatComponentText("§cSelect an item first."));
        } else {
            Minecraft.getMinecraft().thePlayer.addChatComponentMessage(new ChatComponentText("§aItem name:§f " +
                    item.getUnlocalizedName()));
        }
    }

    @Override
    public int getRequiredPermissionLevel() {
        return 0;
    }

    @Override
    public boolean canCommandSenderUseCommand(ICommandSender sender) {
        return true;
    }
}
