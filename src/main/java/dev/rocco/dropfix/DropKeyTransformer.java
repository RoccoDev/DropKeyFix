package dev.rocco.dropfix;

import net.minecraft.launchwrapper.IClassTransformer;
import org.objectweb.asm.*;

public class DropKeyTransformer implements IClassTransformer {
    @Override
    public byte[] transform(String name, String className, byte[] basicClass) {
        if("net.minecraft.client.entity.EntityPlayerSP".equals(className)) {
            ClassReader reader = new ClassReader(basicClass);
            ClassWriter writer = new ClassWriter(reader, ClassWriter.COMPUTE_MAXS | ClassWriter.COMPUTE_FRAMES);
            ClassVisitor visitor = new ClassVisitor(Opcodes.ASM5, writer) {

                @Override
                public MethodVisitor visitMethod(int access, String name, String desc, String signature, String[] exceptions) {
                    MethodVisitor mv = super.visitMethod(access, name, desc, signature, exceptions);

                    if ("func_71040_bB".equals(name))  {
                        return new DropKeyVisitor(mv);
                    }

                    return mv;
                }

            };

            reader.accept(visitor, 0);
            return writer.toByteArray();
        }
        return basicClass;
    }
}
