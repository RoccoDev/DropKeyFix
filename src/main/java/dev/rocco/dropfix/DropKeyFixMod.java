package dev.rocco.dropfix;

import dev.rocco.dropfix.cmd.ConfigCommand;
import dev.rocco.dropfix.cmd.UnlocalizedCommand;
import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.EntityPlayerSP;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ChatComponentText;
import net.minecraftforge.client.ClientCommandHandler;
import net.minecraftforge.common.config.Configuration;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Mod(modid = "dropkeyfix", useMetadata = true, guiFactory = "dev.rocco.dropfix.GuiFactory")
public class DropKeyFixMod {

    static DropKeyConfig config;

    @Mod.EventHandler
    public void onPre(FMLPreInitializationEvent evt) {
        Configuration config = new Configuration(evt.getSuggestedConfigurationFile());
        DropKeyFixMod.config = new DropKeyConfig(config);
    }

    @Mod.EventHandler
    public void onInit(FMLInitializationEvent evt) {
        ClientCommandHandler.instance.registerCommand(new ConfigCommand());
        ClientCommandHandler.instance.registerCommand(new UnlocalizedCommand());
    }

    public static boolean shouldDrop() {
        if(!config.getEnabled()) return true;
        boolean checked = false;

        if(config.getServers().length != 0) {
            if(Minecraft.getMinecraft().getCurrentServerData() == null) return true;
            if(!Arrays.asList(config.getServers()).contains(Minecraft.getMinecraft().getCurrentServerData().serverIP)) {
                return true;
            }
        }
        if(config.getItems().length != 0) {
            EntityPlayerSP player = Minecraft.getMinecraft().thePlayer;

            if(player == null) return true;
            if(player.getHeldItem() == null) return true;
            if(Arrays.asList(config.getItems()).contains(player.getHeldItem().getUnlocalizedName())) {
                alert(player.getHeldItem());
                return false;
            }
            checked = true;
        }
        if(config.getSlots().length != 0) {
            EntityPlayerSP player = Minecraft.getMinecraft().thePlayer;

            if(player == null) return true;
            if(player.inventory == null) return true;

            List<Integer> slots = IntStream.of(config.getSlots()).boxed().collect(Collectors.toList());

            if(slots.contains(player.inventory.currentItem)) {
                alert(player.getHeldItem());
                return false;
            }
            checked = true;
        }
        System.out.println(checked);
        if(!checked) alert(Minecraft.getMinecraft().thePlayer.getHeldItem());
        return checked;
    }

    private static void alert(ItemStack item) {
        if(item == null) return;
        Minecraft.getMinecraft().thePlayer.addChatComponentMessage(new ChatComponentText("§cPrevented you from dropping §c" +
                item.getDisplayName() + "§c (" + item.getUnlocalizedName() + "). §cRun §c/dropsettings §cto §cconfigure."));
    }
}
