package dev.rocco.dropfix;

import net.minecraftforge.common.config.Configuration;
import net.minecraftforge.common.config.Property;

public class DropKeyConfig {

    private Configuration inner;

    // Properties
    private Property enabled, servers, slots, items;

    public DropKeyConfig(Configuration config) {
        this.inner = config;
        load();
    }

    private void load() {
        enabled = inner.get("dkf", "enabled", true, "Whether the mod should prevent you from dropping items.");
        servers = inner.get("dkf", "servers", new String[] {
                "mc.hypixel.net"
        }, "A list of server IPs where the mod should work.");
        slots = inner.get("dkf", "slots", new int[0], "A list of slot ids (starting from 0) where the mod should prevent from dropping.");
        items = inner.get("dkf", "items", new String[0], "A list of item unlocalized names (hold an item and run /dropname) that the mod should prevent from dropping.");
    }

    public Configuration getInner() {
        return inner;
    }

    public boolean getEnabled() {
        return enabled.getBoolean();
    }

    public String[] getServers() {
        return servers.getStringList();
    }

    public int[] getSlots() {
        return slots.getIntList();
    }

    public String[] getItems() {
        return items.getStringList();
    }

    public void save() {
        inner.save();
    }
}
