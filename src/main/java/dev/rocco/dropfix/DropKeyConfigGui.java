package dev.rocco.dropfix;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiScreen;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.config.ConfigElement;
import net.minecraftforge.fml.client.config.GuiConfig;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent;

public class DropKeyConfigGui extends GuiConfig {

    public DropKeyConfigGui(GuiScreen parentScreen) {
        super(parentScreen,
                new ConfigElement(DropKeyFixMod.config.getInner().getCategory("dkf")).getChildElements(),
                "dropkeyfix", false, false,
                GuiConfig.getAbridgedConfigPath(DropKeyFixMod.config.getInner().toString()));
    }

    @SubscribeEvent
    public void onTick(TickEvent.ClientTickEvent evt) {
        MinecraftForge.EVENT_BUS.unregister(this);
        Minecraft.getMinecraft().displayGuiScreen(this);
    }

    @Override
    public void onGuiClosed() {
        super.onGuiClosed();
        DropKeyFixMod.config.save();
    }
}
